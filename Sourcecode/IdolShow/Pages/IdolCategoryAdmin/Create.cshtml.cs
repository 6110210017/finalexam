using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using IdolShow.Data;
using IdolShow.Models;

namespace IdolShow.Pages.IdolCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly IdolShow.Data.IdolShowContext _context;

        public CreateModel(IdolShow.Data.IdolShowContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public IdolCategory IdolCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.IdolCategory.Add(IdolCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}