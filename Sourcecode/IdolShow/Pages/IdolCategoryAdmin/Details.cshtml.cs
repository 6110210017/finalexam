using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolShow.Data;
using IdolShow.Models;

namespace IdolShow.Pages.IdolCategoryAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly IdolShow.Data.IdolShowContext _context;

        public DetailsModel(IdolShow.Data.IdolShowContext context)
        {
            _context = context;
        }

        public IdolCategory IdolCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolCategory = await _context.IdolCategory.FirstOrDefaultAsync(m => m.IdolCategoryID == id);

            if (IdolCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
