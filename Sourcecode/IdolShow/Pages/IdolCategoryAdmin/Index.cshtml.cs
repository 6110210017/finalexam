using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolShow.Data;
using IdolShow.Models;

namespace IdolShow.Pages.IdolCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly IdolShow.Data.IdolShowContext _context;

        public IndexModel(IdolShow.Data.IdolShowContext context)
        {
            _context = context;
        }

        public IList<IdolCategory> IdolCategory { get;set; }

        public async Task OnGetAsync()
        {
            IdolCategory = await _context.IdolCategory.ToListAsync();
        }
    }
}
