using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolShow.Data;
using IdolShow.Models;

namespace IdolShow.Pages.IdolCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly IdolShow.Data.IdolShowContext _context;

        public DeleteModel(IdolShow.Data.IdolShowContext context)
        {
            _context = context;
        }

        [BindProperty]
        public IdolCategory IdolCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolCategory = await _context.IdolCategory.FirstOrDefaultAsync(m => m.IdolCategoryID == id);

            if (IdolCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            IdolCategory = await _context.IdolCategory.FindAsync(id);

            if (IdolCategory != null)
            {
                _context.IdolCategory.Remove(IdolCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
