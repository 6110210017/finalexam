using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IdolShow.Data;
using IdolShow.Models;

namespace IdolShow.Pages.IdolAdmin
{
    public class EditModel : PageModel
    {
        private readonly IdolShow.Data.IdolShowContext _context;

        public EditModel(IdolShow.Data.IdolShowContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Idol Idol { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idol = await _context.idolList
                .Include(i => i.IdolCat)
                .Include(i => i.postUser).FirstOrDefaultAsync(m => m.IdolID == id);

            if (Idol == null)
            {
                return NotFound();
            }
           ViewData["IdolCategoryID"] = new SelectList(_context.IdolCategory, "IdolCategoryID", "ShortName");
           ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Idol).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IdolExists(Idol.IdolID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool IdolExists(int id)
        {
            return _context.idolList.Any(e => e.IdolID == id);
        }
    }
}
