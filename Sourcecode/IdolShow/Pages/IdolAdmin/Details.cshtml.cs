using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolShow.Data;
using IdolShow.Models;

namespace IdolShow.Pages.IdolAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly IdolShow.Data.IdolShowContext _context;

        public DetailsModel(IdolShow.Data.IdolShowContext context)
        {
            _context = context;
        }

        public Idol Idol { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idol = await _context.idolList
                .Include(i => i.IdolCat)
                .Include(i => i.postUser).FirstOrDefaultAsync(m => m.IdolID == id);

            if (Idol == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
