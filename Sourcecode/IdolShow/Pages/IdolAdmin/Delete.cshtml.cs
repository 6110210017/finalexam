using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolShow.Data;
using IdolShow.Models;

namespace IdolShow.Pages.IdolAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly IdolShow.Data.IdolShowContext _context;

        public DeleteModel(IdolShow.Data.IdolShowContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Idol Idol { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idol = await _context.idolList
                .Include(i => i.IdolCat)
                .Include(i => i.postUser).FirstOrDefaultAsync(m => m.IdolID == id);

            if (Idol == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Idol = await _context.idolList.FindAsync(id);

            if (Idol != null)
            {
                _context.idolList.Remove(Idol);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
