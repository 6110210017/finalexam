﻿using IdolShow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace IdolShow.Data
{
	public class IdolShowContext : IdentityDbContext<NewsUser>
	{
		public DbSet<Idol> idolList { get; set; }
		public DbSet<IdolCategory> IdolCategory{ get; set; }
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=IdolShow.db");
		}
	}
}
