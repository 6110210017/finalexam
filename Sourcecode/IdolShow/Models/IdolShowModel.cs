﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace IdolShow.Models
{
	public class NewsUser : IdentityUser{
		public string FirstName { get; set;}
		public string LastName { get; set;}
	}
	public class IdolCategory {
		public int IdolCategoryID { get; set;}
		public string ShortName {get; set;}
		public string FullName { get; set;}
	}
	public class Idol {
		public int IdolID { get; set; }
		public int IdolCategoryID { get; set; }
		public IdolCategory IdolCat { get; set; }
		[DataType(DataType.Date)]
		public string ReportDate { get; set; }
		public string Detail { get; set; }
		public string pay { get; set; }

		public string NewsUserId {get; set;}
		public NewsUser postUser {get; set;}

	}
}
